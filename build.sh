#!/bin/bash

clang++ \
    -Os \
    -target wasm32 \
    --no-standard-libraries \
    -Wl,--export-all \
    -Wl,--no-entry \
    -Wl,--allow-undefined \
    -I. \
    canvas.cpp \
    sierp.cpp \
    -o sierp.wasm
