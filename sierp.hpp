#ifndef _SIERP_HPP_
#define _SIERP_HPP_

#ifdef __cplusplus
extern "C" {
#endif

void sierpinski_initialise();
void sierpinski_draw_frame(unsigned delta_ms);

#ifdef __cplusplus
}
#endif

#endif // !_SIERP_HPP_
