#include <canvas.hpp>

namespace canvas {
constexpr unsigned width = 640;
constexpr unsigned height = 480;
unsigned buffer[width * height] = {0x00};
} // namespace canvas

unsigned canvas_get_width() { return canvas::width; }
unsigned canvas_get_height() { return canvas::height; }
unsigned canvas_get_size() { return sizeof(canvas::buffer); }

void canvas_set_pixel(unsigned x, unsigned y, unsigned color) {
  if (x < canvas::width && y < canvas::height) {
    canvas::buffer[x + y * canvas::width] = color;
  }
}

void canvas_fill(unsigned color) {
  for (unsigned i = 0; i < sizeof(canvas::buffer) / sizeof(canvas::buffer[0]);
       ++i) {
    canvas::buffer[i] = color;
  }
}

unsigned canvas_make_color(unsigned r, unsigned g, unsigned b) {
  return 0xff000000 | ((b & 0xff) << 16) | ((g & 0xff) << 8) |
         ((r & 0xff) << 0);
}

void canvas_clear() { canvas_fill(canvas_make_color(0, 0, 0)); }

void canvas_initialise() { animation_initialise(); }

unsigned *canvas_draw_frame(unsigned delta_ms) {
  canvas_clear();
  animation_draw_frame(delta_ms);
  return canvas::buffer;
}
