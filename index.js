const importObject = {
  env : {
    js_floor : Math.floor,
    js_random : Math.random,
    js_sinf : Math.sin,
    js_cosf : Math.cos,
  }
};

var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');

WebAssembly.instantiateStreaming(fetch("sierp.wasm"), importObject)
    .then((obj) => {
      let wasm_buff = obj.instance.exports.memory.buffer;
      let wasm_exp = obj.instance.exports;

      let canvas_width = wasm_exp.canvas_get_width();
      let canvas_size = wasm_exp.canvas_get_size();

      wasm_exp.canvas_initialise();
        
      let started_at = Date.now();

      let render_frame = () => {
        let prior = started_at;
        started_at = Date.now();
        let delta_ms = started_at - prior;

        // render loop
        let canvas_ptr = wasm_exp.canvas_draw_frame(delta_ms);
        let buf = new Uint8ClampedArray(wasm_buff, canvas_ptr, canvas_size);
        let img_data = new ImageData(buf, canvas_width);
        ctx.putImageData(img_data, 0, 0);

        // request another frame
        window.requestAnimationFrame(render_frame);
      };

      requestAnimationFrame(render_frame);
    });
