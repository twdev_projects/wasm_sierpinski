#include <canvas.hpp>
#include <sierp.hpp>

extern "C" {
extern float js_random();
extern float js_floor(float);
extern float js_sinf(float);
extern float js_cosf(float);
}

namespace {

typedef struct Vertex {
  unsigned x;
  unsigned y;
} Vertex;

Vertex vertices[3];
Vertex rp;

Vertex get_random_point() {
  Vertex v{};
  v.x = js_random() * canvas_get_width();
  v.y = js_random() * canvas_get_height();
  return v;
}

Vertex point_lerp(const Vertex *p1, const Vertex *p2, float pos) {
  Vertex interpolated{};
  interpolated.x = (p2->x + p1->x) * pos;
  interpolated.y = (p2->y + p1->y) * pos;
  return interpolated;
}

void sierpinski_generate(int n, float pos) {
  while (n--) {
    const int vert_idx = js_floor(js_random() * 3);
    const auto *v = &vertices[vert_idx % 3];
    rp = point_lerp(v, &rp, pos);
    canvas_set_pixel(rp.x, rp.y, canvas_make_color(0x11, 0xff, 0x11));
  }
}

float time = 0;

// Update triangle vertices
void update_vertices_position(float t) {
  const float horiz_ampl = 300;
  const float vert_ampl = 180;
  const float horiz_freq = 3;
  const float vert_freq = 2;
  const float vert_delta = 3.14f / 5;

  for (auto &v : vertices) {
    float x = horiz_ampl * js_sinf(horiz_freq * t) + canvas_get_width() / 2;
    float y = vert_ampl * js_cosf(vert_freq * t) + canvas_get_height() / 2;
    v.x = static_cast<unsigned>(x);
    v.y = static_cast<unsigned>(y);
    t += vert_delta;
  }
}

} // namespace

void animation_initialise() {
  update_vertices_position(time);
  rp = get_random_point();
}

void animation_draw_frame(unsigned delta_ms) {
  time += (float)delta_ms / 2000;
  update_vertices_position(time);
  sierpinski_generate(10000, 0.5f);
}
