# WASM Sierpinski's triangle

This repository contains a small demo rendering a Sierpinski's
triangle with C++ code compiled to WASM.

## Building

Invoke provided build script:

    ./build.sh
    
The demo can be executed in the browser:

    python -m http.server
    
    chromium http://localhost:8000/index.html
    
![Sierpinski's triangle](sierp.png)    
    
## More information

More information is available on [twdev.blog](http://twdev.blog/2023/05/wasm_cpp_02).
