#ifndef _CANVAS_HPP_
#define _CANVAS_HPP_

#ifdef __cplusplus
extern "C" {
#endif

unsigned canvas_get_width();
unsigned canvas_get_height();
unsigned canvas_get_size();

void canvas_set_pixel(unsigned x, unsigned y, unsigned color);
void canvas_fill(unsigned color);
unsigned canvas_make_color(unsigned r, unsigned g, unsigned b);

void canvas_clear();

// These two must be defined by the animation code!
// They are called by canvas_* functions.
void animation_initialise();
void animation_draw_frame(unsigned);

void canvas_initialise();
unsigned *canvas_draw_frame(unsigned delta_ms);

#ifdef __cplusplus
}
#endif

#endif // !_CANVAS_HPP_
